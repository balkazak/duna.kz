$(function() {

	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');
		});
		return false;
	});

	var swiper1 = new Swiper('.swiper-1', {
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '.swiper-pagination',
		},
	});

	var swiper2 = new Swiper('.swiper-2', {
		navigation: {
			nextEl: '.sbn2',
			prevEl: '.sbp2',
		},
		slidesPerView: 3,
		breakpoints: {
			768: {
				slidesPerView: 1,
				spaceBetween: 40
			}
		}
	});

	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if (scroll >= 150) {
			$(".nav-container").addClass("bg-white");
		} else {
			$(".nav-container").removeClass("bg-white");
		}
	});

});

